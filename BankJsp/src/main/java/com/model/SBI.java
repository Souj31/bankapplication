package com.model;

public class SBI {
	private int AccountNo;
	private int RAccountNo;
	private int RDAccountNo;
	private String FirstName;
	private String LastName;
	private String Name;
	private String EmailId;
	private String Address;
	private String Gender;
	private String PhoneNo;
	private String Type;
	private String Password;
	private double Balance;
	private String username;
	
	public int getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(int accountno) {
		AccountNo = accountno;
	}
	
	public int getRAccountNo() {
		return RAccountNo;
	}
	public void setRAccountNo(int raccountno) {
		RAccountNo = raccountno;
	}
	
	
	public int getRDAccountNo() {
		return RDAccountNo;
	}
	public void setRDAccountNo(int rDAccountNo) {
		RDAccountNo = rDAccountNo;
	}
	
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String fname) {
		FirstName = fname;
	}

	/*
	 * public String getName() { return Name; } public void setName(String name) {
	 * Name = name; }
	 */
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lname) {
		LastName = lname;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	
	
	public String getPhoneNo() {
		return PhoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}
	public String getType() {
		return Type;
	}

	
	public void setType(String type) {
		Type = type;
	}
	public Double getBalance() {
		return Balance;
	}
	public void setBalance(double balance) {
	       Balance = balance;
	}
	
	
	
	public String getPassword() {
		return this.Password;
	}
	public void setPassword(String password) {
		this.Password = password;
	}
	public String getUserName() {
		return this.username;
	}
	public void setUserName(String username) {
		this.username = username;
	}
	
	public SBI(){
		
	}
	
	SBI(String username, String password){
		this.username = username;
		this.Password = password;
	}
	public void setAmount(double amt) {
		// TODO Auto-generated method stub
		
	}
	

}
