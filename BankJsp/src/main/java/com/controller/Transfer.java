package com.controller;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.model.SBI;
public class Transfer {
	SBI sh = new SBI();
	public static Connection con;
		
		public Transfer() throws Exception {
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/SBIBANK","root","password");
				
		}
	public  String transferMoney(int raccountno,double amt,String username) throws Exception
	{
		ArrayList<Integer> acc = new ArrayList<Integer>();
		
		String sql0 = "select AccountNo from login";
		PreparedStatement st0=con.prepareStatement(sql0);
		ResultSet rs0 = st0.executeQuery();
		while(rs0.next())
		{
			Integer accn = rs0.getInt("AccountNo");
			acc.add(accn);
		}
		if(acc.contains(raccountno)) {
			System.out.println("AccountNo identified!");
			
			String sql = "select Balance from login where Name='"+username+"'";
			PreparedStatement st=con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			
			double bal=0.0;
			try {
		       while(rs.next()) {
			   bal = rs.getDouble("Balance");
		    }
			
				double Withdraw = 0;
				Withdraw =amt;
			
			if(bal <= amt) {
				System.out.println("Sorry you cannot transfer Rs."+amt+". You need to maintain the minimum balance of Rs.1000");
				
			}
			else {
				bal = bal - amt;
				String sql1 = "update login set Balance="+bal+", WithDraw="+amt+" where Name='"+username+"'";
				PreparedStatement st1=con.prepareStatement(sql1);
				st1.execute();
				System.out.println("Amount Debited and Balance updated successfully!");
				
				int accn=0;
				String sql5="select AccountNo from login where Name='"+username+"'";
				PreparedStatement st5 = con.prepareStatement(sql5);
				ResultSet rs5=st5.executeQuery();
				while(rs5.next())
				{
					accn=rs5.getInt("AccountNo");
				}
				String sql4="insert into Transactions(AccountNo,UserName,Operation,Date,Amount,Balance) values (?,?,?,(select now()),?,?)";
				PreparedStatement st4 = con.prepareStatement(sql4);
				st4.setInt(1,accn);
				st4.setString(2, username);
				st4.setString(3,"Transferred");
				st4.setDouble(4,amt);
				st4.setDouble(5, bal);
				st4.execute();
				
				st1.close();
			    st4.close();
			    st5.close();
			    rs5.close();
				
			}
		
		rs.close();
		st.close();
		
			String sql2 = "select Balance from login where AccountNo='"+raccountno+"'";
			PreparedStatement st2=con.prepareStatement(sql2);
			ResultSet rs2 = st2.executeQuery();
			while(rs2.next()) {
				
				bal = rs2.getDouble("Balance");
			}
			bal = bal + amt;

			String sql3 = "update login set Balance="+bal+", Deposit="+amt+" where AccountNo='"+raccountno+"'";
			PreparedStatement st3=con.prepareStatement(sql3);
			st3.execute();
			System.out.println("Amount Credited and Balance updated successfully!");
			st3.close();
			rs2.close();
			st2.close();
			return "Amount Transferred Successfully";	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Invalid AccountNo or AccountNo Does Not Exist");
			return "Invalid AccountNo or AccountNo Does Not Exist";
		}
		
		return null;
	}
}
	
	

