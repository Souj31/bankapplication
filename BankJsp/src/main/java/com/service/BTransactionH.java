package com.service;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/BTransactionH")
public class BTransactionH extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Connection con;
   
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
    		response.setContentType("text/html");
    		PrintWriter out=response.getWriter();
    		String username=request.getParameter("username");
    		
    		HttpSession session=request.getSession();
    		session.getAttribute("info");
    		String usr=(String)session.getAttribute("info");
    		
    		if(usr.equals(username))
    		{
    		Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/SBIBANK","root","password");
    		String sql="select * from Transactions where UserName='"+usr+"'";
    		PreparedStatement st = con.prepareStatement(sql);
    		ResultSet rs =st.executeQuery();
    		//ArrayList
    		request.getRequestDispatcher("Transaction.jsp").include(request, response);
    		out.println("<table align='center' border='0' style='width:100%;height:5px; margin-left:auto;position:absolute;down:90px;  border-collapse:collapse; font-size:100%;'><tr align='center'>"
    				+ "<th>AccountNo</th>"
    				+ "<th>UserNAME</th>"
    				+ "<th>Operation</th>"
    				+ "<th>Date</th>"
    				+ "<th>Amount</th>"
    				+ "<th>Balance</th></tr>");
    		while(rs.next())
    		{
    			
    			//out.println("<h1 align='center' style='font-size:150%;'>Yor Account Details</h1><br><br>");
        		
        		
    		   
        		out.println("<tr align='center' ><td>"+rs.getInt(1)+
         				"</td><td>"+rs.getString(2)+
         				"</td><td>"+rs.getString(3)+
         				"</td><td>"+rs.getString(4)+
         				"</td><td>"+rs.getDouble(5)+
         				"</td><td>"+rs.getDouble(6)+
         				"</td></tr>");
        		//out.println("</table>");
        		out.println("<br>");
        		
        		
         		}
         		
    		out.println("</table>");
    		
    		}
//    			request.getRequestDispatcher("Transaction.jsp").include(request, response);
//    			out.println("<table align='center' border='0' style='width:80%;height:5px; move-down:30px;  border-collapse:collapse; font-size:100%; padding-top:-1000px;'><tr align='center'>"
//        				+ "<th>UserNAME</th>"
//        				+ "<th>Operation</th>"
//        				+ "<th>Amount</th>"
//        				+ "<th>Balance</th></tr>");
//    			
//    			out.println("<tr align='center' ><td>"+rs.getString(1)+
//         				"</td><td>"+rs.getString(2)+
//         				"</td><td>"+rs.getDouble(3)+
//         				"</td><td>"+rs.getDouble(4)+
//         				"</td></tr>");
//    		}
//    		
//    		out.println("</table>");
    	
    		else
    		{
    			request.getRequestDispatcher("Transaction.jsp").include(request, response);
    			out.println("<center><h3 style='color:red'>Invalid credentials</h3></center>");
    		}
    			
    		}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
}