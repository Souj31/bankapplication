package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/Fixed_Balance")
public class Fixed_Balance extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Connection con;
    
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String AccountNo = request.getParameter("accn");
		int accn =Integer.parseInt(AccountNo);
		System.out.println(AccountNo);
		
		PrintWriter out = response.getWriter(); 
		HttpSession session=request.getSession();
	    String Username=(String)session.getAttribute("info");
		System.out.println(Username);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/SBIBANK","root","password");
		ArrayList<Integer> acc = new ArrayList<>();
		String sql5 = "select FDAccountNo from FBalance where UName='"+Username+"'";
		PreparedStatement st5 = con.prepareStatement(sql5);
		ResultSet rs5 = st5.executeQuery();
		while(rs5.next())
		{
			int accno = rs5.getInt("FDAccountNo");
			acc.add(accno);
		}
		st5.close();
		rs5.close();
		
		if(acc.contains(accn))
		{
			
			int facc=0;
			String md = null;
			double bal=0, mamt=0;;

			String sql = "select FDAccountNo, MDate, FDBalance, MAmount from FD where FDAccountNo="+accn+"";
			PreparedStatement st = con.prepareStatement(sql);	
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				facc = rs.getInt("FDAccountNo");
				md = rs.getString("MDate");
				bal = rs.getDouble("FDBalance");
				mamt = rs.getDouble("MAmount");
				
				
			}
			st.close();
			rs.close();

			System.out.println(facc);
			System.out.println(md);
			System.out.println(bal);
			System.out.println(mamt);

			Date currentTime = Calendar.getInstance().getTime(); 
			DateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String date = d.format(currentTime);
			Date D1 = d.parse(date);

			System.out.println("Present time - "+D1);

			Date FDDate = d.parse(md);
			System.out.println("Mature time - "+FDDate);

			if(D1.compareTo(FDDate)>0) {
				String sql0 = "update FBalance inner join FD on FBalance.FDAccountNo=FD.FDAccountNo set FBalance.Bal=FD.MAmount where FBalance.FDAccountNo="+accn;
				System.out.println(sql0);
				PreparedStatement st0 = con.prepareStatement(sql0);		
				//System.out.println(sql0);
				st0.execute();
				st0.close();
				String  sql6 = "select * from FD where FDAccountNo="+accn;
	               PreparedStatement st6 = con.prepareStatement(sql6);
	               ResultSet rs6 = st6.executeQuery();
	               //System.out.println(sql0);
	               while(rs6.next())
	               {
	            	   request.getRequestDispatcher("Fixed_Balance.jsp").include(request, response);
	       			//out.println("<h1 align='center' style='font-size:150%;'>Yor Account Details</h1><br><br>");
	           		out.println("<table align='center' border='0' style='width:80%;height:5px; move-down:30px;  border-collapse:collapse; font-size:100%; padding-top:-1000px;'><tr align='center'><th>FD Account No</th>"
	           				+ "<th>Amount (Rs)</th>"
	           				+ "<th>Age</th>"
	           				+ "<th>ROI (%)</th>"
	           				+ "<th>Current Date</th>"
	           				+ "<th>Matured Date</th>"
	           				+ "<th>FDBalance (Rs)</th></tr>");
	           		
	       		   
	           		out.println("<tr align='center' ><td>"+rs6.getInt(1)+"</td><td>"+rs6.getDouble(3)+
	                				"</td><td>"+rs6.getString(4)+
	                				"</td><td>"+rs6.getInt(5)+
	                				"</td><td>"+rs6.getNString(6)+
	                				"</td><td>"+rs6.getNString(7)+
	                				"</td><td>"+mamt+
	                				"</td></tr>");
	                        
	            		}
	            		
	            		out.println("</table>");
	            		//out.println("")
	            		//st0.close();
	            		rs6.close();
	            		st6.close();
	            		
	               }
			else
			{
				String sql0 = "update FBalance inner join FD on FBalance.FDAccountNo=FD.FDAccountNo set FBalance.Bal=FD.MAmount where FBalance.FDAccountNo="+accn;
				System.out.println(sql0);
				PreparedStatement st0 = con.prepareStatement(sql0);		
				st0.execute();
				st0.close();
				String  sql6 = "select * from FD where FDAccountNo="+accn;
	               PreparedStatement st6 = con.prepareStatement(sql6);
	               ResultSet rs6 = st6.executeQuery();
	               while(rs6.next())
	               {
	            	   request.getRequestDispatcher("Fixed_Balance.jsp").include(request, response);
	       			//out.println("<h1 align='center' style='font-size:150%;'>Yor Account Details</h1><br><br>");
	           		out.println("<table align='center' border='0' style='width:80%;height:5px; move-down:30px;  border-collapse:collapse; font-size:100%; padding-top:-1000px;'><tr align='center'><th>FD Account No</th>"
	           				+ "<th>Amount (Rs)</th>"
	           				+ "<th>Age</th>"
	           				+ "<th>ROI (%)</th>"
	           				+ "<th>Current Date</th>"
	           				+ "<th>Matured Date</th>"
	           				+ "<th>FDBalance (Rs)</th></tr>");
	           		        
	       		   
	           		out.println("<tr align='center' ><td>"+rs6.getInt(1)+"</td><td>"+rs6.getDouble(3)+
	                				"</td><td>"+rs6.getString(4)+
	                				"</td><td>"+rs6.getInt(5)+
	                				"</td><td>"+rs6.getNString(6)+
	                				"</td><td>"+rs6.getNString(7)+
	                				"</td><td>"+bal+
	                				"</td></tr>");
	                        
	            		}
	               out.println("</table>");
	
		          //st0.close();
	               rs6.close();
		          st6.close();
			}

         }
		else
		{
			request.getRequestDispatcher("Fixed_Balance.jsp").include(request, response);
			out.println("<center><h3 style='color:red'>Invalid Account No</h3></center>");
	    }
	}
		catch(Exception e)
		{
			e.printStackTrace();
		}

}
}
