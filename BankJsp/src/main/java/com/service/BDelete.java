package com.service;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.controller.Delete;


@WebServlet("/BDelete")
public class BDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		//String FirstName = request.getParameter("fname");
		//String AccountNo = request.getParameter("accno");
		String username = request.getParameter("username");
		//String password = request.getParameter("password");
		
		
		HttpSession session=request.getSession();
        String Username=(String)session.getAttribute("info");
		System.out.println(Username);
		System.out.println(username);
		
		
		try {
			if(Username.equals(username))
			{
			
			
			Delete sc = new Delete();
			sc.delete(username);
			
           RequestDispatcher rd = request.getRequestDispatcher("Delete.jsp");
			
			
			rd.include(request, response);
			
			out.println("<center><font color = red> Account Closed Successfully <font><center>");
			}
			else
			{
				RequestDispatcher rd = request.getRequestDispatcher("Delete.jsp");
				
				
				rd.include(request, response);
				
				out.println("<center><font color = red> Invalid Credentials <font><center>");
			}
				
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
