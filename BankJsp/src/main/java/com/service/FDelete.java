package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/FDelete")
public class FDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Connection con;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String AccountNo = request.getParameter("accn");
		int accn =Integer.parseInt(AccountNo);
		//System.out.println(username);

		PrintWriter out = response.getWriter(); 
		HttpSession session=request.getSession();
		String Username=(String)session.getAttribute("info");
		System.out.println(Username);
		try {
			Class.forName("com.mysql.jdbc.Driver");

			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/SBIBANK","root","password");
			ArrayList<Integer> acc = new ArrayList<>();
			String sql5 = "select FDAccountNo from FD where Name='"+Username+"'";
			PreparedStatement st5 = con.prepareStatement(sql5);
			ResultSet rs5 = st5.executeQuery();
			System.out.println(sql5);
			while(rs5.next())
			{
				int accno = rs5.getInt("FDAccountNo");
				acc.add(accno);
			}
		st5.close();
		rs5.close();

			if(acc.contains(accn))
			{   
				double bal0=0.0;
				double bal1=0.0;

				String sql0 = "select Bal from FBalance where FDAccountNo='"+accn+"'";
				PreparedStatement st0=con.prepareStatement(sql0);
				ResultSet rs0 = st0.executeQuery();
				while(rs0.next())
				{
					bal0=rs0.getDouble("Bal");
				}

				String sql7="select Balance from login where Name='"+Username+"'";
				PreparedStatement st7 =con.prepareStatement(sql7);
				ResultSet rs7 = st7.executeQuery();
				while(rs7.next())
				{
					bal1=rs7.getDouble("Balance");
				}
				if(bal0==0)
				{
					request.getRequestDispatcher("FDelete.jsp").include(request, response);
					out.println("<center><h3 style='color:red'>FD Account Balance is Null</h3></center>");
				}
				else
				{
					bal1= bal0 + bal1;
					System.out.println(bal1);
					String sql8="update login set Balance="+bal1+" where Name='"+Username+"'";
					PreparedStatement st8 = con.prepareStatement(sql8);
					st8.execute();

					String sql="delete from FD where FDAccountNo="+accn+"";
					PreparedStatement st = con.prepareStatement(sql);
					st.execute();
					String sql1="delete from FBalance where FDAccountNo="+accn+"";
					PreparedStatement st1 = con.prepareStatement(sql1);
					st1.execute();
					request.getRequestDispatcher("FDelete.jsp").include(request, response);
					out.println("<center><h3 style='color:green'>Account Closed Successfully</h3></center>");
					st8.close();
					st1.close();
				}
				st0.close();
				rs0.close();
				st7.close();
				rs7.close();
			}
			else
			{
				request.getRequestDispatcher("FDelete.jsp").include(request, response);
				out.println("<center><h3 style='color:red'>Invalid Account No</h3></center>");
			}
			st5.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
}
