package com.service;

import java.io.IOException;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.controller.Update;


@WebServlet("/BUpdate")
public class BUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String username = request.getParameter("username");
		//String password = request.getParameter("password");
		String FirstName = request.getParameter("fname");
		String LastName = request.getParameter("lname");
		String Address = request.getParameter("address");
		String EmailId = request.getParameter("emailid");
		String Phoneno = request.getParameter("phoneno");
		
		HttpSession session=request.getSession();
        String Username=(String)session.getAttribute("info");
		System.out.println(Username);
		
		
        try {
        	if(Username.equals(username))
    		{
			   Update sc = new Update();
			//System.out.println(FirstName);
			sc.update1(FirstName, LastName,Address,EmailId,Phoneno,Username);
			RequestDispatcher rd = request.getRequestDispatcher("Update.jsp");
			rd.include(request, response);
			out.println("<center><font color = red>Updated  successfully<font><center>");
			
        }
        else
        {
        RequestDispatcher rd = request.getRequestDispatcher("Update.jsp");
		rd.include(request, response);
			out.println("<center><font color = red>Invlaid Uesrname<font><center>");
        }
		
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
