package com.service;


import java.io.IOException;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.controller.Deposit;


@WebServlet("/BDeposit")
public class BDeposit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Connection con;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String AccountNo = request.getParameter("accno");
		String username = request.getParameter("username");
		//String password = request.getParameter("password");
		String Amount = request.getParameter("amt");
		double amt = Double.parseDouble(Amount);
		
		
		HttpSession session=request.getSession();
	    String Username=(String)session.getAttribute("info");
		System.out.println(Username);
		
		try {
		if(Username.equals(username))
		{
		    Deposit sc = new Deposit();
			String s= sc.deposit(Username,amt);
			RequestDispatcher rd = request.getRequestDispatcher("Deposit.jsp");
			rd.include(request, response);
			out.println("<center><font color = red>"+s+"<font><center>");
		}
		else
			{
			  RequestDispatcher rd = request.getRequestDispatcher("Deposit.jsp");
			  rd.include(request, response);
			  out.println("<center><font color = red>Invalid Username<font><center>");
			}
		}
			catch(Exception e)
		    {
			e.printStackTrace();
		    }			
	}
}

