package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.controller.Transfer;


@WebServlet("/BTransfer")
public class BTransfer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String username = request.getParameter("username");
		String RAccountNo = request.getParameter("raccno");
		int raccno = Integer.parseInt(RAccountNo);
		
		//String password = request.getParameter("password");
		String Amount = request.getParameter("amt");
		double amt = Double.parseDouble(Amount);
	
		
		HttpSession session=request.getSession();
        String Username=(String)session.getAttribute("info");
		//System.out.println(Username);
		
		if(Username.equals(username))
			
		{
			System.out.println(username);
		   Transfer  sc = new Transfer();
		   //String s=sc.transferMoney(Integer.parseInt(RAccountNo), amt,Username);
		   String s=sc.transferMoney(raccno, amt,username);
			
            RequestDispatcher rd = request.getRequestDispatcher("Transfer.jsp");
			rd.include(request, response);
			out.println("<center><font color = red>"+s+"<font><center>");
		}
		else
		{
			RequestDispatcher rd = request.getRequestDispatcher("Transfer.jsp");
			rd.include(request, response);
			out.println("<center><font color = red>Invalid Username<font><center>");
		}
			
				
		} 
		catch (SQLException e) 
		{
	      e.printStackTrace();
		}
		
		catch(Exception e) {
			System.out.println(e);
		}
	}

}
