<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fixed Deposit Form</title>

<style>
Header{
display:flex;
justify-content:flex-end;
margin-top:-800px;
margin-left:1300px;
padding:30px 90px;
}
button{
padding:9px 25px;
cursor:pointer;
border:none;
}
</style>


    <!-- CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Fixed.css" />

    <!-- Boxicons CSS -->
    <link
      href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css"
      rel="stylesheet"
    />
</head>


<div class="container" style="position:absolute;left:700px;">
     <div class="container-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
      <form action="BFD" method="post">
       
        <div class="field">
          <div class="input-field">
            <input type="number" name="amt" placeholder="Enter your amount" class="amt" />
          </div>
          <br>
       
        <div class="age-field" >Select  Age : 
          <input type = "radio" name = "age" value="senior" required="required" onclick="rd(0)"> Senior citizen &nbsp;&nbsp;
         <input type = "radio" name = "age" value="non_senior" required="required" onclick="rd(1)"> Non Senior Citizen &nbsp;&nbsp;
        <br><br>
        </div>
          
          <div id="seniorC" hidden>Rate of Interest : 
            <input type = "radio" name = "rate" value="6" required="required"> 6% - 2min&nbsp;&nbsp;
            <input type = "radio" name = "rate" value="7" required="required"> 7% - 4min&nbsp;&nbsp;
            <input type = "radio" name = "rate" value="9" required="required"> 9% - 6min&nbsp;&nbsp;
         </div>
        
         <div id="non" hidden>Rate of Interest : 
          <input type = "radio" name = "rate" value="5" required="required"> 5% - 2min &nbsp;&nbsp;
          <input type = "radio" name = "rate" value="6" required="required"> 6% - 4min &nbsp;&nbsp;
          <input type = "radio" name = "rate" value="8" required="required"> 8% - 6min&nbsp;&nbsp;
         </div>
         
       
        <div class="input-field button">
          <input type="submit" value="Submit Now" />
        </div>
      </form>
    </div>
    </div>
<Header>
<a href="BDashboard.jsp">
<button style="background-color:  #185bf5; padding-left:15px; margin-left:100px; color: white; border-radius: 8px; height: 50px; width: 120px; font-size: 18px; border: none;">Dashboard</button>
</a>

<a href="home.jsp">
<button style="background-color:  #185bf5; margin-left:30px;color: white; border-radius: 8px; height: 50px; width: 100px; font-size: 18px; border: none;">Logout</button>
</a>
</Header>

<script>
function rd(x){
	if(x==0){
		document.getElementById("seniorC").style.display = "block";
		document.getElementById("non").style.display = "none";
	}
	else{
		document.getElementById("non").style.display = "block";
		document.getElementById("seniorC").style.display = "none";
	}
	return;
}
</script>
</body>
</html>

