<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Form</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/Blogin.css">

<style>
header{
display:flex;
justify-content:flex-end;
align-items:right;
padding:30px 90px;
}
button{
padding:9px 25px;
cursor:pointer;
border:none;
}
</style>
</head>

<body>
 <Header>
<a href="register.jsp">
<button style="background-color: #185bf5 ; padding-left:15px; margin-left:100px; color: white; border-radius: 8px; height: 50px; width: 120px; font-size: 18px; border: none;">Sign Up</button>
</a>

<a href="home.jsp">
<button style="background-color:#185bf5 ; margin-left:30px;color: white; border-radius: 8px; height: 50px; width: 100px; font-size: 18px; border: none;">Home</button>
</a>
</Header>
	
    <div class="Login-form">
        <form action ="BLogin" method="post">
            <div class="form-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
            
            <div class="form-group">
                <input type="text" name="username" class="form-control item" id="username" placeholder="Username" required="required">
            </div>
            <br>
            
            <div class="form-group">
                <input type="text"name="password" class="form-control item" id="password"  placeholder="Enter your Password" required="required">
            </div>
            <br>
           
            <div class="form-group">
                <button type="submit" class="btn btn-block signin">Sign in</button>
            </div>
        </form>
        
         <div class="social-media">
            <h5></h5>
            <div class="social-icons">
                
            </div>
        </div>
    </div>
</body>
</html>
