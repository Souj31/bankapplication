<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registration Form Form</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/BRegister.css">

<style>
header{
display:flex;
justify-content:flex-end;
align-items:right;
padding:30px 90px;
}
button{
padding:9px 25px;
cursor:pointer;
border:none;
}
</style>
</head>

<body>
 <Header>
<a href="login.jsp">
<button style="background-color:#185bf5; padding-left:15px; margin-left:100px; color: white; border-radius: 8px; height: 50px; width: 120px; font-size: 18px; border: none;">Sign In</button>
</a>

<a href="home.jsp">
<button style="background-color:#185bf5; margin-left:30px;color: white; border-radius: 8px; height: 50px; width: 100px; font-size: 18px; border: none;">Logout</button>
</a>
</header>
	
    <div class="Register-form">
        <form action ="BRegister" method="post">
            <div class="form-icon">
                <span><i class="icon icon-user"></i></span>
            </div>
            
            <div class="form-group">
                <input type="text" name="fname" class="form-control item" id="name" placeholder=" Enter your FirstName" required="required">
            </div>
            
            <div class="form-group">
                <input type="text" name="lname" class="form-control item" id="name" placeholder="Enter your LastName" required="required">
            </div>
           
            
            <div class="form-group">
                <input type="email" name="emailid" class="form-control item" id="emailid" placeholder="Enter your EmailId" required="required">
            </div>
           
            
            <div class="form-group">
                <input type="text" name="address" class="form-control item" id="address" rows="5" cols="20" placeholder="Enter your Address" required="required">
            </div>
           
            
             <div class="form-group">
                <input type="text" name="gender" class="form-control item" id="gender" placeholder="Enter your Gender" required="required">
            </div>
            
            
             <div class="form-group">
                <input type="text" name="type" class="form-control item" id="type" placeholder="Enter your Account Type" required="required">
            </div>
           
            
             <div class="form-group">
                <input type="tel" name="phoneno" class="form-control item" id="phoneno" placeholder="Enter your Phone No" required="required">
            </div>
           
            <div class="form-group">
                <input type="number" name="amt" class="form-control item" id="amount" min="500" oninput="this.value = Math.abs(this.value)" max="999999" placeholder="Enter Amount" required="required">
            </div>
            
            
             <div class="form-group">
                <input type="text" name="username" class="form-control item" id="username" placeholder="Enter your User Name" required="required">
            </div>
           
            
             <div class="form-group">
                <input type="password" name="pwd" class="form-control item" id="pwd" placeholder="Enter your Password" required="required">
            </div>
            
           
            <div class="form-group">
                <button type="submit" class="btn btn-block Register">Register</button>
            </div>
        </form>
        
         <div class="social-media">
            <h5></h5>
            <div class="social-icons">
                
            </div>
        </div>
    </div>
</body>
</html>
