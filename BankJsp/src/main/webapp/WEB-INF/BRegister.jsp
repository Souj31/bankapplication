<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Sign Up Form</title>

<!-- Font Icon -->
<link rel="stylesheet"
	href="fonts/material-icon/css/material-design-iconic-font.min.css">

<!-- Main css -->
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action ="BRegister">

	<div class="main">

		<!-- Sign up form -->
		<section class="signup">
			<div class="container">
				<div class="signup-content">
					<div class="signup-form">
						<h2 class="form-title">Sign up</h2>
					
						<form method="" action="BRegister" class="register-form"
							id="register-form">
							<div class="form-group">
								<label for="name"></label> <input
									type="text" name="fname" id="name" placeholder="Your FirstName" required="required" />
							</div>
							<div class="form-group">
								<label for="name"></label> <input
									type="text" name="lname" id="name" placeholder="Your LastName" required="required"/>
							</div>
							<div class="form-group">
								<label for="email"></label> <input
									type="email" name="email" id="email" placeholder="Your Email" required="required"/>
							</div>
							
							<div class="form-group">
							     <label for="address"></label> <input type="address" name="address" rows="5" cols="20" id="address" placeholder="Address" required="required"/>
							</div>
							
							<div class="form-group">
							     <label for="gender"></label> <input type="text" name="gender" id="gender" placeholder="gender" required="required"/>
							  </div>
							  
							  <div class="form-group">
							  <label for ="selecttype"></label> <input type="text" name="type" id="type" placeholder="Accounttype" required="required"/>
                              </div>
                              
                              <div class="form-group">
								<label for="amount"></label> <input
									type="Number" name="bal"  min="0" id="amount" placeholder="amount" required="required" />
							</div>
							
							<div class="form-group">
								<label for="contact"></label>
								<input type="tel" name="phoneno" id="phoneno"
									placeholder="Contact no" required="required"/>
							</div>
							
							<div class="form-group">
								<label for="username"></label> <input type = "text" name = "user"  id="pass" placeholder="username" required="required"/>
							</div>
							
							
							
							<div class="form-group">
								<label for="pass"></label> <input
									type="password" name="pwd" id="pwd" placeholder="Password" required="required" />
							</div>
							
							
							
							<div class="form-group form-button">
								<input type="submit" name="signup" id="signup"
									class="form-submit" value="Register" />
							</div>
						</form>
					</div>
					<div class="signup-image">
						<figure>
							<img src="https://cdni.iconscout.com/illustration/premium/thumb/free-registration-desk-1886554-1598085.png" alt="sing up image">
						</figure>
						<a href="BLogin.html" class="signup-image-link">I am already member</a>
					</div>
				</div>
			</div>
		</section>


	</div>
	<!-- JS -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="js/main.js"></script>


</form>
</body>

</html>