<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Indian Bank</title>
        <link rel="shortcut icon" href="assets/images/fav.png" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
        <link rel="shortcut icon" href="assets/images/fav.jpg">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
        <link rel="stylesheet"  href="css/home.css" />
    </head>
  <body>
    <div class="nav-col bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 pt-1 pb-2 align-items-center">
                            <img class="max-230" src="assets/images/Fauget_page-0001.jpg" alt="">
                            <a  data-bs-toggle="collapse" data-bs-target="#menu" class="float-end d-lg-none pt-1 ps-3"><i class="bi pt-1 fs-1 cp bi-list"></i></a>
                        </div>
                        <div id="menu" class="col-lg-9 d-none d-lg-block">
                            <ul class="float-end mul d-inline-block">
                                <li class="float-md-start p-4">Home</a></li>
                                <li class="float-md-start p-4">About US</a></li>
                                <li class="float-md-start p-4">Services</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-conten big-padding">
            <div class="container py-3">
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <h1 class="fs-11 dfr fw-bold">Connect All your <span class="text-primary">Banking Needs</span> and Grow </h1>
                        <div class=" d-inline-block pt-4">
                         <a href="login.jsp">
                            <button class="btn btn-light shadow-md p-3 px-5 fs-7 fw-bold">Sign In</button>
                            </a>
                            <a href="register.jsp">
                            <button class="btn btn-primary ms-3 fs-7 shadow-md p-3 px-5 fw-bold">Sign Up</button>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 pt-4">
                        <img class="w-100" src="assets/images/slid.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid big-padding bg-white about-cover">
        <div class="container">
            <div class="row about-row">
                <div class="col-md-6 no-padding image">
                    <img src="assets/images/a1.jpg" alt="">
                </div>
                <div class="col-md-6 detail text-justify ps-4 ">
                    <h2>About</h2>
                    <p>Private bank Indian Bank was founded on september 25th 2022 and has its headquarters in Bangalore, Karnataka India.</p>
                    <p>Through our network of locations in Karnataka, we offer a variety of banking products. One zonal office and three regional hubs are scattered throughout Karnataka's major cities.The Total Staff Strength is about 200.</p>
                     
                        <p> Contact No: 08035068.</p>
                       
                </div>
            </div>
        </div>
    </div>
        
         <div class="what-we-do big-padding bg-white container-fluid">
           <div class="container">
               <div class="section-title row">
                   <h2>Our Services</h2>
                   
               </div>
               <div class="row mt-4 whd">
                   <div class="col-md-4 text-start mb-5">
                       <i class="bi text-primary fs-1 bi-cash"></i>
                       <h4 class="fs-6 fw-bold mt-3">WithDraw</h4>
                       
                   </div>
                   <div class="col-md-4 text-start mb-5">
                       <i class="bi fs-1 text-primary bi-piggy-bank"></i>
                       <h4 class="fs-6 fw-bold mt-3">Deposit</h4>
                       
                   </div>
                    <div class="col-md-4 text-start mb-5">
                       <i class="bi fs-1 text-primary bi-arrow-left-right"></i>
                       <h4 class="fs-6 fw-bold mt-3">Transfer</h4>
                       
                   </div>
                    <div class="col-md-4 text-start mb-5">
                      <i class="bi fs-1 text-primary bi-wallet2"></i>
                       <h4 class="fs-6 fw-bold mt-3">Fixed Deposit</h4>
                       
                   </div>
                     <div class="col-md-4 text-start mb-5">
                      <i class="bi fs-1 text-primary bi-person-fill"></i>
                       <h4 class="fs-6 fw-bold mt-3">Profile Updation</h4>
                     
                   </div>
                    
                     <div class="col-md-4 text-start mb-5">
                       <i class="bi  fs-1 text-primary bi-shield-shaded"></i>
                       <h4 class="fs-6 fw-bold mt-3">FD Account</h4>
                      
                   </div>
                   
                  
               </div>
           </div>
       </div>
       
      
   
    <div class="mobile-cart container-fluid big-padding">
        <div class="container">
           <div class="row">
               <div class="col-md-8 align-self-center">
                    <h4 class="fs-2 fw-bold mb-3">Stay connected,  <br>enjoy peace of mind on the Go</h4>
                    
                    <div class="btnrow mt-4">
                    </div>
                </div>
                <div class="col-md-4 mt-4">
                    <img src="assets/images/case/c6.jpeg" alt="">
                </div>
           </div>
            
        </div>
    </div>
    
    
                
                </div>
                </div>
            </div>
        </div>
        </body>
        </html>