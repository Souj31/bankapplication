package com.test;

import static org.junit.Assert.assertEquals;




import org.junit.Assert;
import org.junit.Test;

import com.controller.Login;
import com.controller.Register;
import com.controller.Transfer;
import com.model.SBI;

public class WTest {
	
	@Test
	public void validcredentials() throws Exception {
		Login obj=new Login();
		String result=null;
		String username="souj";
		String password="123";
		result=obj.login(username, password);
		Assert.assertEquals("Login Successful", result);
		System.out.println(result);
		}
	
	@Test
	public void invalidusername() throws Exception {
		Login obj=new Login();
		String result=null;
		String username="soujanya";
		String password="123";
		result=obj.login(username, password);
		Assert.assertEquals("Invalid Username", result);
		System.out.println(result);
		}
	
	
	@Test
	public void invalidpassword() throws Exception {
		Login obj=new Login();
		String result=null;
		String username="souj";
		String password="678";
		result=obj.login(username, password);
		Assert.assertEquals("Password is incorrect", result);
		System.out.println(result);
		}
	
	@Test
	public void registerexisting() throws Exception {
		Register obj=new Register();
		String result=null;
		String username="souj";
		String fname="tre",lname="fre",emailid="tgsf",address="jhgd",gender="f",phoneno="teyhdg",type="fdg",password="yeryw",balance="7654";
		result=obj.createAccount(fname, lname, username, emailid, address, gender, phoneno, type, balance, password);
		Assert.assertEquals("UserName Already Exists.PLease Enter a new one", result);
		System.out.println(result);
		}
	
	
	@Test
	public void reciveraccountexists() throws Exception {
		Transfer obj=new Transfer();
		String result=null;
		int raccno=10;
		String username="tref";
		Double amt=0.0;
		String fname="tre",lname="fre",emailid="tgsf",address="jhgd",gender="f",phoneno="teyhdg",type="fdg",password="yeryw",balance="7654";
		result=obj.transferMoney(raccno, amt,username);
		Assert.assertEquals("Invalid AccountNo or AccountNo Does Not Exist", result);
		System.out.println(result);
		}
}

